import java.awt.Color;

public enum Etat {
	NORMAL(Color.YELLOW),
	SURVOLE(Color.BLUE),
	SELECTIONNE(Color.MAGENTA),
	DESACTIVE(Color.GRAY);
	
	private Color fond;
	
	private Etat(Color c) {
		fond = c;
	}
	
	public Color getFond() {
		return fond;
	}
}
