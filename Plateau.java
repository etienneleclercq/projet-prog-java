import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JComponent;

public class Plateau extends JComponent implements ActionListener, KeyListener{
	Vaisseau vaisseau;
	
	public Plateau() {
		super();
		vaisseau = new Vaisseau();
		setPreferredSize(new Dimension(Constantes.Largeur_fenetre,Constantes.Hauteur_fenetre));
	}
	
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2 = (Graphics2D)g;
		vaisseau.affiche(g2);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		vaisseau.deplacer();
		repaint();
	}

	@Override
	
	public void keyTyped(KeyEvent e) {
		
	}

	@Override
	public void keyPressed(KeyEvent e) {
		switch(e.getKeyCode()) {
//			case KeyEvent.VK_SPACE:
//				Projectile.tirer();
//			break;
			case KeyEvent.VK_LEFT:
				System.out.println("Go !!!");
				vaisseau.seDeplacerGauche();
				repaint();
			break;
			case KeyEvent.VK_RIGHT:
				vaisseau.seDeplacerDroite();
				repaint();
			break;
		}
	}


	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}
}
