import java.awt.FlowLayout;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.*;

public class SpaceInvaders implements KeyListener{
	Plateau plateau;
	private Timer timer;
	
	public SpaceInvaders() {
		plateau = new Plateau();
		
		timer = new Timer(40,plateau); // 1e argument : temps en ms, 2e argument le actionListener
		
		JFrame f = new JFrame();
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.setLayout(new FlowLayout(FlowLayout.CENTER));
		f.add(plateau);
		f.addKeyListener(this);
		f.addKeyListener(plateau);
		f.pack();
		f.setLocationRelativeTo(null);
		f.setVisible(true);
	}
	
	public static void main(String[] args) {
		new SpaceInvaders();
	}

	@Override
	public void keyTyped(KeyEvent e) {
		
	}

	@Override
	public void keyPressed(KeyEvent e) {
		switch(e.getKeyCode()) {
			case KeyEvent.VK_LEFT:
			case KeyEvent.VK_RIGHT:
				timer.start();
			break;
			case KeyEvent.VK_BACK_SPACE:
				timer.stop();
			break;
		}
	}


	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}
}
