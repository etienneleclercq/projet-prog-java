import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Rectangle2D;

public class Vaisseau {
	Ellipse2D.Double vaisseau;
	int sens;
	
	public Vaisseau() {
		vaisseau = new Ellipse2D.Double(Constantes.X_POS_INIT_VAISSEAU, Constantes.Y_POS_VAISSEAU,100,25);
		sens = 1;
	}
	
	public void deplacer() {
		 this.vaisseau.x+=(Constantes.Pas*sens);
	}
	

    public void seDeplacerDroite() {
       sens=1;
    }
	
    public void seDeplacerGauche() {
        sens=-1;
	}
	public void affiche(Graphics2D g2) {
		g2.setColor(Color.pink);
		g2.fill(vaisseau);
		g2.setColor(Color.BLACK);
		g2.draw(vaisseau);
	}
}


   